import React, { Component } from 'react';

import Filtering from './components/Filtering/Filtering';
import ArticleList from './components/ArticleList/ArticleList';
import './App.scss';

class App extends Component {
    render() {
        return (
            <div className="App">
                <h1 className="title">
                    Articles page
                </h1>
                <Filtering />
                <ArticleList />
            </div>
        );
    }
}

export default App;
