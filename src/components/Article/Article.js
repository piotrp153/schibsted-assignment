import React from 'react';

import './Article.scss';

const article = props => {

    const imageStyles = {
        backgroundImage: `url(${ props.image })`
    }


    return (
        <div className="article">
            <div className="article__image" style={ imageStyles }></div>
            <div className="article__info">
                <div className="article__top-info">
                    <h2 className="article__title">
                        { props.title }
                    </h2>
                    <p className="article__date">
                        { props.date }
                    </p>
                </div>
                <p className="article__desc">
                    { props.preamble }
                </p>
            </div>
        </div>
    )
}

export default article;