import React, { Component } from 'react';
import { connect } from 'react-redux';
import ConfigMiddleware from '../../store/middleware';

import Article from '../Article/Article';
import './ArticleList.scss';

class ArticleList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            articles: this.props.articles,
            error: this.props.error
        }
    }


    componentDidMount() {
        const { fetchArticles } = this.props;
        fetchArticles();
    }

    render() {
        const { articles } = this.props;

        let posts = null;

        if( articles) {
            posts = articles.map( article => {
                return <Article 
                            key = { article.id } 
                            image = { article.image }
                            title = { article.title } 
                            date = { article.date }
                            preamble = { article.preamble }
                        />
            });
        }

        return (
            <div className="artlicles">
                { this.props.error ? <div className="error-message">Sorry something went wrong!</div> : null }
                { articles ? posts : null }
            </div>
        )
    }
}


const mapStateToProps = state => ({
    articles: state.articles.articles,
    error: state.articles.error
})

const mapDispatchToProps = dispatch => ({
    fetchArticles: () => dispatch(ConfigMiddleware.fetchArticles(['sports'])),
});


export default connect(
    mapStateToProps,
    mapDispatchToProps
)( ArticleList );