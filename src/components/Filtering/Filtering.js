import React, { Component } from 'react';
import { connect } from 'react-redux';
import ConfigMiddleware from '../../store/middleware';

import './Filtering.scss';

class Filtering extends Component {
    constructor(props) {
        super(props);

        this.state = {
            articles: this.props.articles.articles,
            filtering: [{id: 1, name: 'sports', active: true}, {id: 2, name: 'fashion', active: false}],
            sorting: '',
        }
    }

    changeFiltering(elementIndex) {
        const array = [...this.state.filtering];
        const names = [];

        array[elementIndex].active = !array[elementIndex].active;

        this.setState({filtering: array, sorting: ''});

        this.state.filtering.map(item => {
            if(item.active) {
                return names.push(item.name);
            }
            return false;
        });

        this.props.fetchArticles(names);
    }

    changeSorting() {
        const months = [ "januar", "februar", "märz", "april", "mai", "juni", "juli", "august", "september", "oktober", "november", "desember" ];
        let array = [...this.props.articles];
        let sortingType = '';


        this.props.articles.map((item, indexOf ) => {
            const date = item.date.split(' ');

            date[0] = date[0].slice(0, -1);

            months.map( (item, indexOf) => {
                if(item === date[1]) {
                    return date[1] = indexOf + 1;
                }
                return false;
            })

            const newDate = new Date(date[2] + '-' + date[1] + '-' + date[0]);

            return array[indexOf] = {...array[indexOf], formatedDate: newDate};
        })

        let newArray = array.sort((a, b) => a.formatedDate - b.formatedDate);

        if(this.state.sorting === 'ASC'){
            sortingType = 'DESC';
            newArray = newArray.reverse();
        } else {
            sortingType = 'ASC';
        } 

        this.setState({ articles: newArray, sorting: sortingType });
        this.props.sortArticles(newArray);
    }
    
    render() {
        const filteringList = this.state.filtering.map( (item, indexOf) => {
            return <li className={"list__item " + ( item.active ? 'active' : null ) } key={ item.id } onClick={() => this.changeFiltering( indexOf )}>
                { item.name }
            </li>
        }); 
        
        return(
            <div className="filtering">
                <div className="filtering__filter">
                    <h3 className="filtering__title">
                        Sort by: 
                    </h3>
                    <ul className="filtering__list">
                        { filteringList }
                    </ul>
                </div>

                <div className={"filtering__sorting " +  this.state.sorting.toLocaleLowerCase() } onClick={ () => this.changeSorting() }>
                    <p>
                        Sort by date
                    </p>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    articles: state.articles.articles
})

const mapDispatchToProps = dispatch => ({
    fetchArticles: elements => dispatch(ConfigMiddleware.fetchArticles(elements)),
    sortArticles: articles => dispatch(ConfigMiddleware.sortArticles(articles))
});


export default connect(
    mapStateToProps,
    mapDispatchToProps
)( Filtering );