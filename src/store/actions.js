export const FETCH_ARTICLES_SUCCESS = 'FETCH_ARTICLES_SUCCESS';
export const FETCH_ARTICLES_ERROR = 'FETCH_ARTICLES_ERROR';

export function fetchArticlesSuccess(articles) {
    return {
        type: FETCH_ARTICLES_SUCCESS,
        payload: articles
    }
}

export function fetchArticlesError(error) {
    return {
        type: FETCH_ARTICLES_ERROR,
        payload: error
    }
}
