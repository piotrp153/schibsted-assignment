import { fetchArticlesSuccess, fetchArticlesError } from './actions';
import axios from 'axios';

export default class ConfigMiddleware {
    static fetchArticles = endpoint => async dispatch => {
        try {
            const articles = [];
            
            const anAsyncFunction = async item => {
                return await axios.get('http://localhost:6010/articles/' + item)
                .then(
                    response => response,
	                error => error
                );
            }
            
            const getData = async () => {
                return await Promise.all(endpoint.map(item => anAsyncFunction(item)))
            }

            const data = getData();

            data.then(function(value){
                value.map( item => {
                    if(item instanceof Error){
                        dispatch(fetchArticlesError(item));
                    } else {
                        item.data.articles.map( item => {
                            return articles.push(item);
                        } )
                    }
                    return false;
                });
                dispatch(fetchArticlesSuccess(articles));
            });

        } catch (error) {
            dispatch(fetchArticlesError(error));
        }
    }
    static sortArticles = articles => dispatch => {
        dispatch(fetchArticlesSuccess(articles))
    }
} 

