import {FETCH_ARTICLES_SUCCESS, FETCH_ARTICLES_ERROR} from './actions';

export const initialState = {
    articles: [],
    error: null
}

export function articlesReducer(state = initialState, action) {
    switch(action.type) {
        case FETCH_ARTICLES_SUCCESS:
            return {
                ...state,
                articles: action.payload
            }
        case FETCH_ARTICLES_ERROR:
            return {
                ...state,
                error: action.payload,
            }
        default: 
            return state;
    }
}